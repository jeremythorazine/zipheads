<?php
/* 
Plugin Name: Music Custom Post Types
Plugin URI: http://www.jeremybasolo.com 
Description: Music Custom Post Types needed
Version: 1.0
Author: Jeremy Basolo
Author URI: http://www.jeremybasolo.com
*/

/*  Copyright 2011 Jeremy Basolo (email : jeremy.thorazine@gmail.Com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


// Gig custom post type and taxonomy
add_action('init', 'gig_init');

function gig_init() {
	
	// create the labels
	$labels = array(
		'name' => _x('Gigs', 'post type general name'),
		'singular_name' => _x('Gig', 'post type singular name'),
		'add_new' => _x('Add New', 'Gig item'),
		'add_new_item' => __('Add a new Gig'),
		'edit_item' => __('Edit Gig'),
		'new_item' => __('New Gig'),
		'view_item' => __('View Gig'),
		'search_items' => __('Search Gigs'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
	
	// create the args
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'has_archive' => true,
		'supports' => array('title','editor','thumbnail')
	);
	
	// register the post type
	register_post_type( 'gig' , $args );

}

// add meta box
add_action('admin_init', 'gig_admin_init');

function gig_admin_init() {
  add_meta_box("gigs-details", "Gig Details", 'gig_details', 'gig', 'normal', 'low');
}

// Add meta boxes:
function gig_details(){
	global $post;	
	$custom = get_post_custom($post->ID);
	$gig_link = $custom["gig_link"][0];
	$gig_location = $custom["gig_location"][0];
	$gig_gmap = $custom["gig_gmap"][0];
	wp_nonce_field('rvmeta_save', 'rvmeta_save' );
	?>	
	<label>Link: </label>
	<p><input style='width:300px;' name='gig_link' value='<?php echo $gig_link; ?>' /></p>
  <label>Location: </label>
	<p><input style='width:300px;' name='gig_location' value='<?php echo $gig_location; ?>' /></p>
	<label>Google Maps: </label>
	<p><input style='width:300px;' name='gig_gmap' value='<?php echo $gig_gmap; ?>' /></p>
  <?php 
}

add_action('save_post', 'gig_save_post');

function gig_save_post() {
	global $post;
	
	if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
	  return;
	
	if(!wp_verify_nonce($_POST['rvmeta_save'], 'rvmeta_save' ))
	  return;
	
	if (array_key_exists('gig_link', $_POST)) {
		update_post_meta($post->ID, "gig_link", $_POST["gig_link"]);
	}
	if (array_key_exists('gig_location', $_POST)) {
		update_post_meta($post->ID, "gig_location", $_POST["gig_location"]);
	}
	if (array_key_exists('gig_gmap', $_POST)) {
		update_post_meta($post->ID, "gig_gmap", $_POST["gig_gmap"]);
	}
}

// IN THE THEME, USE post_status=future TO QUERY THE UPCOMING GIGS. OTHERWISE, UNCOMMENT THE BELOW LINE
//add_filter( 'wp_insert_post_data', 'futurenow_do_not_set_posts_to_future' );
        
function futurenow_do_not_set_posts_to_future( $data ) {
    if ( $data['post_status'] == 'future' && $data['post_type'] == 'gig' )
        $data['post_status'] = 'publish';
    return $data;
}

// Add video menu page
add_action( 'admin_menu', 'register_video_menu_page' );
function register_video_menu_page() {
  add_menu_page('Videos', 'Videos', 'administrator', 'video-settings', 'video_settings');
}
function video_settings() {
  if(isset($_POST['video_links']) && !empty($_POST['video_links'])) {
    update_option('video_links', $_POST['video_links']);
  }
  $video_links = get_option('video_links');
  ?>
  <div class="wrap">
    <div id="icon-options-general" class="icon32"><br></div>
    <h2>Video Links</h2>
    <form action="admin.php?page=video-settings" method="post">
    <p>One Youtube link per line:</p>
    <textarea name="video_links" cols="60" rows="20"><?php echo $video_links; ?></textarea>
    <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
    </form>
  </div>
  <?php
}

// Gig custom post type and taxonomy
add_action('init', 'gallery_init');

function gallery_init() {
	
	// create the labels
	$labels = array(
		'name' => _x('Galleries', 'post type general name'),
		'singular_name' => _x('Gallery', 'post type singular name'),
		'add_new' => _x('Add New', 'Gallery item'),
		'add_new_item' => __('Add a new Gallery'),
		'edit_item' => __('Edit Gallery'),
		'new_item' => __('New Gallery'),
		'view_item' => __('View Gallery'),
		'search_items' => __('Search Galleries'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
	
	// create the args
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'has_archive' => true,
		'supports' => array('title','editor','thumbnail')
	);
	
	// register the post type
	register_post_type( 'gallery' , $args );

}

?>