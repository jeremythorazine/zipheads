<?php
/**
 * The standard page template file
 *
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$templates = array( 'page.twig' );

Timber::render( $templates, $context );
