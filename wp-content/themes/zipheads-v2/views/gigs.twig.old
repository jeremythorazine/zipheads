{% extends "base.twig" %}

{% block content %}

	<div class="container">
		<div class="row">
			<section id="left-content">
				<header>
			        <h1>{{ current_year ? 'Gigs of ' ~ current_year : 'Upcoming Gigs' }}</h1>
			    </header>
			    {% if gigs %}
					{% for gig in gigs %}
					    <article class="gig">
							<div class="row">
						    	<div class="content">
						    		<header>
								    	<h2><span class="date">{{ gig.event_date|date('d/m/Y') }}</span> - {{ gig.title }}</h2>
							    	</header>
						    		{{ gig.content }}
						    		<footer>
							    		{% if gig.event_location['address']|trim %}
							    			<p><strong>Location: </strong>{{ gig.event_location['address'] }}</p>
							    		{% endif %}
							    		{% if gig.event_link or gig.event_location %}
							    			<div class="links">
												{% if gig.event_location %}
									    			<a target="_blank" rel="nofollow" href="https://www.google.com/maps/?q={{ gig.event_location['address'] }}">VIEW ON MAP</a>
									    		{% endif %}
												{% if gig.event_link %}
									    			<a target="_blank" rel="nofollow" href="{{ gig.event_link }}">MORE INFO</a>
									    		{% endif %}
								    		</div>
							    		{% endif %}
							    	</footer>
						    	</div>
						    	{% if gig.thumbnail %}
							    	<div class="flyer">
							    		{% set image = TimberImage(gig.thumbnail) %}
							    		<a href="{{ image.src('full') }}"><img src="{{ image.src('flyer') }}"></a>
							    	</div>
							    {% endif %}
					    	</div>
					    </article>
					{% endfor %}
				{% else %}
					No upcoming gigs at the moment..
					<br>
					Check back soon or try to book us!
			    {% endif %}
			</section>
			<aside id="right-content">
				{% if current_year %}
					<div class="module">
						<div class="content">
							<a href="{{ post.link }}" class="btn btn-default">Back to gigs</a>
						</div>
					</div>
				{% else %}
					<div class="module archive">
						<h3>Past Gigs</h3>
						<div class="content">
							<ul>
								{% for year in years %}
									<li><a href="{{ post.link }}?y={{ year }}">{{ year }}</a></li>
								{% endfor %}
							</ul>
						</div>
					</div>
				{% endif %}
			</aside>
		</div>
	</div>

{% endblock %}
