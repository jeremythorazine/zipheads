<?php
/**
 * Template Name: Gigs
 *
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['gigs'] = get_data_from_songkick();

$i = 0;
foreach($context['gigs']['event'] as $gig) {
	$line_up = "";
	foreach($gig['performance'] as $perf) {
		if($perf['billing'] == 'headline') {
			$context['gigs']['event'][$i]['headline'] = $perf['artist']['displayName'];
		}
		$line_up .= $perf['artist']['displayName'] . ", ";
	}
	$context['gigs']['event'][$i]['line_up'] = substr($line_up, 0, strlen($line_up) - 2);
	$i++;
}

$templates = array( 'gigs.twig' );

Timber::render( $templates, $context );
