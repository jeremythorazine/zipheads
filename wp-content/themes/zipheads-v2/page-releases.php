<?php
/**
 * Template Name: Releases
 *
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$releases = array(
	'post_type' => 'release',
	'posts_per_page' => -1,
	'meta_key' => 'date_of_release',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
);

$context['releases'] = Timber::get_posts($releases);

$templates = array( 'releases.twig' );

Timber::render( $templates, $context );
