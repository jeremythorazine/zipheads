<?php
/**
 * Template Name: Music
 *
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$templates = array( 'music.twig' );

Timber::render( $templates, $context );
