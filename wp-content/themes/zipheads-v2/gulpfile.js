// ===========================================================================================================
// Gulp config file 
// Author: Jeremy Basolo
// Version: 1.0
// ===========================================================================================================

//   ██████╗  ██████╗     ███╗   ██╗ ██████╗ ████████╗    ███████╗██████╗ ██╗████████╗
//   ██╔══██╗██╔═══██╗    ████╗  ██║██╔═══██╗╚══██╔══╝    ██╔════╝██╔══██╗██║╚══██╔══╝
//   ██║  ██║██║   ██║    ██╔██╗ ██║██║   ██║   ██║       █████╗  ██║  ██║██║   ██║
//   ██║  ██║██║   ██║    ██║╚██╗██║██║   ██║   ██║       ██╔══╝  ██║  ██║██║   ██║
//   ██████╔╝╚██████╔╝    ██║ ╚████║╚██████╔╝   ██║       ███████╗██████╔╝██║   ██║
//   ╚═════╝  ╚═════╝     ╚═╝  ╚═══╝ ╚═════╝    ╚═╝       ╚══════╝╚═════╝ ╚═╝   ╚═╝
//
//   ██╗   ██╗███╗   ██╗██╗     ███████╗███████╗███████╗    ██╗   ██╗ ██████╗ ██╗   ██╗
//   ██║   ██║████╗  ██║██║     ██╔════╝██╔════╝██╔════╝    ╚██╗ ██╔╝██╔═══██╗██║   ██║
//   ██║   ██║██╔██╗ ██║██║     █████╗  ███████╗███████╗     ╚████╔╝ ██║   ██║██║   ██║
//   ██║   ██║██║╚██╗██║██║     ██╔══╝  ╚════██║╚════██║      ╚██╔╝  ██║   ██║██║   ██║
//   ╚██████╔╝██║ ╚████║███████╗███████╗███████║███████║       ██║   ╚██████╔╝╚██████╔╝
//    ╚═════╝ ╚═╝  ╚═══╝╚══════╝╚══════╝╚══════╝╚══════╝       ╚═╝    ╚═════╝  ╚═════╝
//
//   ██╗  ██╗███╗   ██╗ ██████╗ ██╗    ██╗    ██╗    ██╗██╗  ██╗ █████╗ ████████╗
//   ██║ ██╔╝████╗  ██║██╔═══██╗██║    ██║    ██║    ██║██║  ██║██╔══██╗╚══██╔══╝
//   █████╔╝ ██╔██╗ ██║██║   ██║██║ █╗ ██║    ██║ █╗ ██║███████║███████║   ██║
//   ██╔═██╗ ██║╚██╗██║██║   ██║██║███╗██║    ██║███╗██║██╔══██║██╔══██║   ██║
//   ██║  ██╗██║ ╚████║╚██████╔╝╚███╔███╔╝    ╚███╔███╔╝██║  ██║██║  ██║   ██║
//   ╚═╝  ╚═╝╚═╝  ╚═══╝ ╚═════╝  ╚══╝╚══╝      ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝
//
//   ██╗   ██╗ ██████╗ ██╗   ██╗     █████╗ ██████╗ ███████╗    ██████╗  ██████╗ ██╗███╗   ██╗ ██████╗
//   ╚██╗ ██╔╝██╔═══██╗██║   ██║    ██╔══██╗██╔══██╗██╔════╝    ██╔══██╗██╔═══██╗██║████╗  ██║██╔════╝
//    ╚████╔╝ ██║   ██║██║   ██║    ███████║██████╔╝█████╗      ██║  ██║██║   ██║██║██╔██╗ ██║██║  ███╗
//     ╚██╔╝  ██║   ██║██║   ██║    ██╔══██║██╔══██╗██╔══╝      ██║  ██║██║   ██║██║██║╚██╗██║██║   ██║
//      ██║   ╚██████╔╝╚██████╔╝    ██║  ██║██║  ██║███████╗    ██████╔╝╚██████╔╝██║██║ ╚████║╚██████╔╝
//      ╚═╝    ╚═════╝  ╚═════╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝    ╚═════╝  ╚═════╝ ╚═╝╚═╝  ╚═══╝ ╚═════╝


// ===========================================================================================================
// VARIABLES
// ===========================================================================================================

// Global File paths

var config = {
	nodePath:      'node_modules/',
	nodePathAll:   'node_modules/**/*',
	jsPath:         './library/js',
	jsPathAll:      './library/js/*.js',
	temp:           './library/temp',
	scssPath:       './library/scss',
	scssPathAll:    './library/scss/**/*.scss',
	imgPath:        './build/images',
	destImg:        './build/images/*.{gif,png,jpg,jpeg,svg}',
	dest:           './build',
	destCss:        './build/css',
	destJs:         './build/js'
};

// Add the JS files here

var jsFileList = [
    config.nodePath + 'bootstrap-sass/assets/javascripts/bootstrap.js',
    config.nodePath + 'lightgallery/dist/js/lightgallery.js',
    config.nodePath + 'lightgallery/dist/js/lg-zoom.js',
    config.nodePath + 'lightgallery/dist/js/lg-video.js',
    config.nodePath + 'lightgallery/dist/js/lg-autoplay.js',
    // config.nodePath + 'lightgallery/dist/js/lg-hash.js',
    config.nodePath + '@mizchi/jquery-pjax/jquery.pjax.js',
    config.nodePath + 'slick-carousel/slick/slick.js',
	config.jsPath + '/music.js',
	config.jsPath + '/scripts.js'
];

// Styles paths

var scssFilePaths = [
    config.nodePath + 'bootstrap-sass/assets/stylesheets/',
    config.nodePath + '@fortawesome/fontawesome-free/scss/',
    config.nodePath + 'lightgallery/dist/css/',
    config.nodePath + 'slick-carousel/slick/',
    config.nodePath + 'hamburgers/_sass/hamburgers/'
];

// Load the Gulp plugins

var gulp = require('gulp'); // Load the Gulp core
var runSequence = require('run-sequence'); // Load as this isn't gulp based
var buster = require('gulp-asset-hash'); // Load as this didn't work :P

// Load all the plugins by referring to package.json

var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins();


// ===========================================================================================================
// TASKS
//
// Sequencing became necessary because we only want to lint scripts.js (not every script!)
// Also, we want to fold Modernizr into the concatenated script file for deployment
//
// If there is a better solution then fill your boots
// ===========================================================================================================

gulp.task('default', function() {
	runSequence('modernizr', 'lint', 'scripts', 'styles');
});


// Styles task

gulp.task('styles', function () {
	return gulp.src([config.scssPath + '/styles.scss']) // Base scss include
		.pipe(plugins.plumber(function(error) {
			errorHandler:reportError
		}))
		.pipe(plugins.sass({
			outputstyle: 'compressed',
            includePaths: scssFilePaths
		}))
		.on('error', reportError)
		.pipe(plugins.autoprefixer({
			cascade: false
		}))
		.pipe(plugins.rename('styles.min.css'))
		// .pipe(plugins.bless())
		.pipe(plugins.combineMq())
		.pipe(plugins.minifyCss({
			keepSpecialComments : 0
		}))
		.pipe(gulp.dest(config.destCss))
		.pipe(buster.hash({
			manifest: './build/manifest.json',
			template: '<%= name %>.<%= ext %>'
		}))
		.pipe(gulp.dest(config.destCss))
});

// Scripts task

gulp.task('scripts', function () {
	return gulp.src(jsFileList)
		.pipe(plugins.concat({
			path: config.destJs + '/scripts.js',
			cwd: ''
		}))
		.pipe(plugins.rename('scripts.min.js'))
		.pipe(plugins.uglify())
		.pipe(gulp.dest(config.destJs))
		.pipe(buster.hash({
			manifest: './build/manifest.json',
			template: '<%= name %>.<%= ext %>'
		}))
		.pipe(gulp.dest(config.destJs))
});

// Linting task

gulp.task('lint', function(){
	return gulp.src('js/scripts.js')
		.pipe(plugins.jshint())
		.pipe(plugins.plumber(function(error) {
			errorHandler:reportError
		}))
		.pipe(plugins.jshint.reporter('default'))
		.on('error', reportError)
});

// Modernizr task

gulp.task('modernizr', function() {
	return gulp.src(config.jsPathAll)
		.pipe(plugins.modernizr({
			tests : [
				'backdropfilter',
		        'backgroundsize',
		        'bgpositionxy',
		        'bgrepeatspace_bgrepeatround',
		        'bgsizecover',
		        'borderradius',
		        'cssanimations',
		        'cssfilters',
		        'csspseudoanimations',
		        'csspseudotransitions',
		        'cssremunit',
				'csstransforms3d',
		        'csstransforms',
		        'csstransitions',
		        'devicemotion_deviceorientation',
		        'flexbox',
		        'fontface',
		        'nthchild',
		        'opacity',
		        'overflowscrolling',
		        'rgba',
		        'svg',
		        'svgasimg',
		        'svgfilters',
		        'touchevents',
		        'xhrresponsetypejson',
		        'domprefixes',
		        'setclasses',
		        'shiv'
			],
			options: [
				'setClasses'
			]
		}))
		.pipe(gulp.dest(config.destJs))
});

// Images task

gulp.task('images', function () {
	return gulp.src(config.imgPath)
		.pipe(plugins.cache(plugins.imagemin({
			optimizationLevel: 3,
			progressive: false,
			interlaced: false
		})))
		.pipe(gulp.dest(config.imgPath))
});

// Watch task

gulp.task('watch', function () {
	gulp.watch(config.scssPathAll, ['styles']);
	gulp.watch(config.destImg, ['images']);

	// Run the scripts task in the correct sequence

	gulp.watch(config.jsPathAll, function() {
		runSequence('lint','scripts');
	});

});


// ===========================================================================================================
// HELPER FUNCTIONS
// ===========================================================================================================

// Error reporter function

var reportError = function (error) {
    var lineNumber = (error.lineNumber) ? 'LINE ' + error.lineNumber + ' -- ' : '';

    var report = '';
    var chalk = plugins.util.colors.yellow.bgRed;

    report += chalk('😢 ') + ' [' + error.plugin + ']\n';
    report += chalk('😢 ') + ' ' + error.message + '\n\n';

    if (error.lineNumber) {
		report += chalk('LINE:') + ' ' + error.lineNumber + '\n';
	}

    if (error.fileName) {
		report += chalk('FILE:') + ' ' + error.fileName + '\n';
	}

    console.error(report);
    this.emit('end'); // Stop the watch task from ending
}
