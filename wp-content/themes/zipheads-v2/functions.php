<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

require_once( 'includes/core.php' ); // This is all the core base functions in one place.
require_once( 'includes/scripts-styles.php' ); // Enqueue scripts and styles.
require_once( 'includes/pagination.php' ); // Add pagination function.
require_once( 'includes/custom-post-types.php' ); // Create useful custom post types. Can be changed or removed
require_once( 'includes/default-pages.php' ); // Create default pages on theme activation (and only the first time).
require_once( 'includes/timber.php' ); // All the necessary timber stuff.
require_once( 'includes/various.php' ); // Copy and paste various crappy code from StackOverflow in there...
