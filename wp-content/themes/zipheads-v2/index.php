<?php
/**
 * Blog page template
 *
 */

$context = Timber::get_context();


$context['posts'] = Timber::get_posts();
$context['pagination'] = Timber::get_pagination();  

$templates = array( 'news.twig' );

Timber::render( $templates, $context );
