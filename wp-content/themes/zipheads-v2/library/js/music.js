 jQuery(document).ready(function($) {

    if($('#audio-container').length > 0) {

        // Init variables

        var playingState = function(state) {
            if(state == 1) {
                $('#audio-player').data('playing', 1);
                $('#audio-container').addClass('playing');
                $('#audio-container .controls .play').html('<i class="fa fa-pause" aria-hidden="true">');
            } else {
                $('#audio-player').data('playing', 0);
                $('#audio-container').removeClass('playing');
                $('#audio-container .controls .play').html('<i class="fa fa-play" aria-hidden="true">');
            }
        }

        var adjustProgressAndTime = function () {
            $('#audio-container .controls .current-time').text(player.currentTime.toString().toMMSS());
            var percentage = player.currentTime * 100 / player.duration;
            $('#audio-container .progress-bar .play-bar').css('width', percentage.toString() + '%');
        }

        var container = $('#audio-container');
        var player = $('#audio-player')[0];
        player.src = $('#audio-container .playlist .playlist-item:first-child').attr('href');

        // On song load
        player.addEventListener('loadedmetadata', function() {
            $('#audio-container .controls .duration').text(player.duration.toString().toMMSS());
            adjustProgressAndTime();
        });

        // On time change
        player.addEventListener('timeupdate', function() {
            adjustProgressAndTime();
        });

        player.load();
        $('#audio-container .playlist .playlist-item:first-child').addClass('current');
        $('#audio-player').data('playing', 0);



        // Playlist item click

        $('body').on('click', '#audio-container .playlist-item', function(ev) {
            ev.preventDefault();
            player.src = $(this).attr('href');
            player.load();
            player.play();
            $('#audio-container .playlist-item').removeClass('current');
            $(this).addClass('current');
            playingState(1);
        });



        // Play / Pause
        $('body').on('click', '#audio-container .controls .play', function(ev) {
            ev.preventDefault();
            if($('#audio-player').data('playing') == 1) {
                player.pause();
                playingState(0);
            } else {
                player.play();
                playingState(1);
            }
        });



        // Stop
        $('body').on('click', '#audio-container .controls .stop', function(ev) {
            ev.preventDefault();
            player.src = player.src;
            player.load();
            playingState(0);
        });



        // Next
        $('body').on('click', '#audio-container .controls .next', function(ev) {
            ev.preventDefault();
            var nextItem = $('#audio-container .playlist-item.current').next();
            if(nextItem.length > 0) {
                player.src = nextItem.attr('href');
                player.load();
                player.play();
                playingState(1);
                $('#audio-container .playlist-item').removeClass('current');
                nextItem.addClass('current');
            }
        });



        // Prev
        $('body').on('click', '#audio-container .controls .previous', function(ev) {
            ev.preventDefault();
            var prevItem = $('#audio-container .playlist-item.current').prev();
            if(prevItem.length > 0) {
                player.src = prevItem.attr('href');
                player.load();
                player.play();
                playingState(1);
                $('#audio-container .playlist-item').removeClass('current');
                prevItem.addClass('current');
            }
        });


        // Seek bar
        $('body').on('click', '#audio-container .progress-bar .seek-bar', function(ev) {
            ev.preventDefault();
            var pixels = ev.pageX-$(this).offset().left;
            var width = $(this).width();
            player.currentTime = pixels * player.duration / width;
        });


}

});

String.prototype.toMMSS = function () {
    var sec_num = Math.floor(parseFloat(this, 10)); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num / 60));
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return minutes+':'+seconds;
}