
/* jshint asi: true */

var videoPlayer = null;
function onYouTubePlayerAPIReady() {
    videoPlayer = new YT.Player('video-iframe');
    // $('#video-list').fadeIn(250);
}

jQuery(document).ready(function($) {

    $('#content-container').addClass('on');

    // Home slider
    $('#slider').slick({
        responsive: [
            {
                breakpoint: 767,
                settings: "unslick"
            }
        ]
    });

    // Heads in the header
    $('#header .heads img').hover(function() {
        $(this).attr('src', $(this).attr('src').replace('.gif', '-hover.gif'));
    }, function() {
        $(this).attr('src', $(this).attr('src').replace('-hover.gif', '.gif'));
    });

    // Navigation toggle on mobile 
    $('#header .nav-container .hamburger').click(function(ev) {
        ev.preventDefault();
        $(this).toggleClass('is-active');
        $('html, body').animate({
            scrollTop: $('#header .nav-container').offset().top - 5
        }, 500);
        $('#header .nav-container .main-nav').slideToggle();
    });

    // Lightgallery
    $("#image-gallery, #instagram-feed .row").lightGallery({
        thumbnail: false,
        zoom: true,
        actualSize: false
    });

    $(".slide .video-link").lightGallery({
        counter: false,
        zoom: false,
        actualSize: false,
        videoMaxWidth: '85%'
    });

    $(".gig .flyer").lightGallery({
        thumbnail: false,
        zoom: true,
        actualSize: false,
        counter: false
    });

    $('.next-gig .flyer').on('click', function(ev) {
        ev.preventDefault();
        var imageSrc = $(this).attr('href');
        $(this).lightGallery({
            thumbnail: false,
            zoom: true,
            actualSize: false,
            autoPlay: false,
            dynamic: true,
            dynamicEl: [{
                'src': imageSrc
            }],
            counter: false
        });
    });

    // Video Gallery

    // if($('#video-player').length > 0) {
    //     videoPlayer = new YT.Player('video-iframe', {});
    // }

    // if($('#video-list').length > 0) {
    //     $('#video-list').hide();
    // }
    $('body').on('click', '#video-list .video-link a', function(ev) {
        ev.preventDefault();
        if($(this).hasClass('current')) {
            return false;
        }
        if(videoPlayer == null) {
            videoPlayer = new YT.Player('video-iframe');
        }
        var videoID = $(this).data('id');
        if(/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
            videoPlayer.cueVideoById(videoID);
        } else {
            videoPlayer.loadVideoById(videoID);
        }
        $('#video-list .video-link a').removeClass('current');
        $(this).addClass('current');
        $('html, body').animate({
            scrollTop: $('#video-player').offset().top -20
        }, 250);
    });

    var createSubMenu = function(liContainer, current) {
        var subNavHTML = '<div id="secondary-nav-holder" ';
        subNavHTML = subNavHTML + 'style="display: none;"';
        subNavHTML = subNavHTML + ' data-item="' + liContainer.attr('id') + '">';
        subNavHTML = subNavHTML + '<ul>';
        subNavHTML = subNavHTML + $('.sub-menu', liContainer).html();
        subNavHTML = subNavHTML + '</ul>';
        subNavHTML = subNavHTML + '</div>';
        $('#header .nav-container').after(subNavHTML);
        $('#header #secondary-nav-holder').fadeIn(250);
        if(current) {
            $('#header #secondary-nav-holder').addClass('current');
        }

        var liOffsetLeft = liContainer.offset();
        liOffsetLeft = liOffsetLeft.left;
        var subNavOffsetLeft = $('#secondary-nav-holder li:first-child').offset();
        var offsetLeft = liOffsetLeft + (liContainer.width() / 2);
        offsetLeft = offsetLeft - ($('a', liContainer).css('padding-left').replace('px', '') * 2);
        $('#secondary-nav-holder li:first-child').css('margin-left', offsetLeft);
    }

    $('body').on('click', '#header .main-nav li a', function(e) {
        var liContainer = $(this).closest('li');
        if(liContainer.hasClass('menu-item-has-children')) {
            e.preventDefault();
            if($('#secondary-nav-holder').length > 0) {
                return false;  
            }
            var current = false;
            if(liContainer.hasClass('current-menu-item') || liContainer.hasClass('current-menu-ancestor')) current = true;
            if($('body').hasClass('single-gallery') && liContainer.hasClass('media-menu-item')) current = true;
            createSubMenu(liContainer, current);
        }
    });


    // if($('.single-gallery #header .main-nav .media-menu-item').length > 0) {
    //     setTimeout(createSubMenu($('#header .main-nav .media-menu-item'), true), 1000);
    // }

    // if($('#header .main-nav .current-menu-item.menu-item-has-children').length > 0) {
    //     setTimeout(createSubMenu($('#header .main-nav .current-menu-item.menu-item-has-children'), true), 1000);
    // }

    // if($('#header .main-nav .current-menu-ancestor.menu-item-has-children').length > 0) {
    //     setTimeout(createSubMenu($('#header .main-nav .current-menu-ancestor'), true), 1000);
    // }


    // $('#video-list').slick({
    //     dots: false,
    //     slidesToShow: 4,
    //     slidesToScroll: 2,
    //     infinite: false,
    //     width: '100%',
    //     responsive: [{
    //         breakpoint: 768,
    //         settings: {
    //             slidesToShow: 2,
    //             slidesToScroll: 2
    //         }
    //     }]
    // });

    // Share Functionality
    $('body').on( 'click', '.share a:not(.mail)', function(ev) {
        if($(window).width() > 600) {
            ev.preventDefault();
            window.open($(this).attr('href'), '', 'width=600,height=350');
        }
    });


    // Gigs page
    if($('#right-content').length > 0) {
        var initialOffset = $('#right-content').offset();
        var adjustTop = function() {
            if($('#right-content').height() < $(window).height()) {
                if($('#right-content').length > 0 && $('#right-content').css('float') == 'left') {
                    if ($(window).scrollTop() > initialOffset.top) {
                        $('#right-content').css('margin-top', ($(window).scrollTop() - initialOffset.top + 15) + 'px');
                    } else {
                        $('#right-content').css('margin-top', 0);
                    }
                }
            }
        }
        $(window).scroll(function() {
            adjustTop();
        });
        adjustTop();
    }


});
