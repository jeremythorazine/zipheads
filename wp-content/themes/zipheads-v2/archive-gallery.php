<?php
/**
 * The standard page template file
 *
 */

$context = Timber::get_context();

$galleries = array(
	'post_type' => 'gallery',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC'
);

$context['posts'] = Timber::get_posts($galleries);

$templates = array( 'galleries.twig' );

Timber::render( $templates, $context );
