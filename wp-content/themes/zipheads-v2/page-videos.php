<?php
/**
 * Template Name: Videos
 *
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$videos_field = get_field('videos');
$videos = array();

foreach($videos_field as $video) {
	$video['id'] = get_youtube_id($video['youtube_url']);
	$video['title'] = $video['video_title'];
	$details = youtube_api_items('videos', array(
		'id' => $video['id'],
		'part' => 'snippet,contentDetails'
	));
	if($details[0]['snippet']) {
		$video['thumbnail'] = $details[0]['snippet']['thumbnails']['medium']['url'];
		if(empty($video['video_title'])) {
			$video['title'] = $details[0]['snippet']['title'];		
		}
	}
	$videos[] = $video;
}

$context['videos'] = $videos;
$current_video = $videos[0]['id'];
if(isset($_GET['v'])) {
	$current_video = filter_var($_GET['v'], FILTER_SANITIZE_STRING);
}
$context['current_video'] = $current_video;
$templates = array( 'videos.twig' );

Timber::render( $templates, $context );
