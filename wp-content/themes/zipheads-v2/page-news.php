<?php
/**
 * Blog page template
 *
 */

global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}
$context = Timber::get_context();
$args = array(
    'post_type' => '[post]',
    'posts_per_page' => 6,
    'paged' => $paged
);

// For pagination!
query_posts($args);

$context['posts'] = Timber::get_posts();
$context['pagination'] = Timber::get_pagination();  

$templates = array( 'news.twig' );

Timber::render( $templates, $context );
