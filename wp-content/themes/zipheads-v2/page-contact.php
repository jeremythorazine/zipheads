<?php
/**
 * Template Name: Contact
 *
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$templates = array( 'contact.twig' );

Timber::render( $templates, $context );
