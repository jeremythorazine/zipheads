<?php

// Gig custom post type and taxonomy

add_action('init', 'gig_init');

function gig_init() {
    
    // create the labels
    $labels = array(
        'name' => _x('Gigs', 'post type general name'),
        'singular_name' => _x('Gig', 'post type singular name'),
        'add_new' => _x('Add New', 'Gig item'),
        'add_new_item' => __('Add a new Gig'),
        'edit_item' => __('Edit Gig'),
        'new_item' => __('New Gig'),
        'view_item' => __('View Gig'),
        'search_items' => __('Search Gigs'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );
    
    // create the args
    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-calendar-alt',
        'has_archive' => false,
        'supports' => array('title','editor','thumbnail')
    );
    
    // register the post type
    register_post_type( 'gig' , $args );

}

// Release custom post type

add_action('init', 'release_init');

function release_init() {
    
    // create the labels
    $labels = array(
        'name' => _x('Releases', 'post type general name'),
        'singular_name' => _x('Release', 'post type singular name'),
        'add_new' => _x('Add New', 'Release item'),
        'add_new_item' => __('Add a new Release'),
        'edit_item' => __('Edit Release'),
        'new_item' => __('New Release'),
        'view_item' => __('View Release'),
        'search_items' => __('Search Releases'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );
    
    // create the args
    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-album',
        'has_archive' => false,
        'supports' => array('title','editor','thumbnail')
    );
    
    // register the post type
    register_post_type( 'release' , $args );

}



// Gallery

function gallery_post_type() {

        $labels = array(
                'name'                  => _x( 'Galleries', 'Post Type General Name', 'text_domain' ),
                'singular_name'         => _x( 'Gallery', 'Post Type Singular Name', 'text_domain' ),
                'menu_name'             => __( 'Galleries', 'text_domain' ),
                'name_admin_bar'        => __( 'Galleries', 'text_domain' ),
                'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
                'all_items'             => __( 'All Galleries', 'text_domain' ),
                'add_new_item'          => __( 'Add New Gallery', 'text_domain' ),
                'add_new'               => __( 'Add New', 'text_domain' ),
                'new_item'              => __( 'New Item', 'text_domain' ),
                'edit_item'             => __( 'Edit Item', 'text_domain' ),
                'update_item'           => __( 'Update Item', 'text_domain' ),
                'view_item'             => __( 'View Item', 'text_domain' ),
                'search_items'          => __( 'Search Item', 'text_domain' ),
                'not_found'             => __( 'Not found', 'text_domain' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
                'items_list'            => __( 'Items list', 'text_domain' ),
                'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
                'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
        );
        $args = array(
                'label'                 => __( 'Galleries', 'text_domain' ),
                'description'           => __( 'Galleries Custom Post', 'text_domain' ),
                'labels'                => $labels,
                'supports'                      => array( 'title', 'editor', 'thumbnail'),
                'taxonomies'            => array( ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 20,
                'menu_icon'             => 'dashicons-images-alt',
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
        );
        register_post_type( 'gallery', $args );

}
add_action( 'init', 'gallery_post_type', 0 );
