<?php

Timber::$dirname = array('views');


add_filter( 'timber_context', 'add_to_context' );
add_filter( 'get_twig', 'add_to_twig' );


// Register variables for using in twig template file & Add to context so it can be accessed globally.
function add_to_context( $context ) {
	$content[''] = new TimberMenu('');
	//globally assigned theme options
	$context['domain'] = get_bloginfo('url');
	$context['ajax_url'] = admin_url('admin-ajax.php');
	$context['extra_links'] = get_field('extra_links', 'option');
	$context['header_image'] = get_field('header_image', 'option');
	$context['footer_terms'] = get_field('footer_terms', 'option');
	$context['menu'] = new TimberMenu();
	return $context;
}

function add_to_twig( $twig ) {
	/* this is where you can add your own fuctions to twig */
	$twig->addExtension( new Twig_Extension_StringLoader() );
	$twig->addFilter( 'str_repeat', new Twig_Filter_Function( 'str_repeat' ) );
	$twig->addFilter( 'youtube_id', new Twig_Filter_Function( 'get_youtube_id' ) );
	return $twig;
}

