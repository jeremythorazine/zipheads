<?php

//******************************************************************************
// VARIOUS STUFF BELOW
//******************************************************************************

// Used in twig as a filter. Ex: {{ post.video_url | youtube_id }}
function get_youtube_id($url) {
	if(preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches)) {
		return $matches[0];
	}
	return false;
}

// To query the Youtube V3 data api
function youtube_api($end_point, $parameters) {
	$api_key = get_field('google_api_key', 'option');
	$url = 'https://www.googleapis.com/youtube/v3/' . $end_point. '/?';
	$param_array = array();
	foreach($parameters as $parameter_key => $parameter_val) {
		$param_array[] = $parameter_key . '=' . $parameter_val;
	}
	$url .= implode('&', $param_array);
	$url .= '&key=' . $api_key;
	$youtube_results = wp_remote_get($url);
	return json_decode($youtube_results['body'], true);	
}

function youtube_api_items($end_point, $parameters) {
	$transient_name = '_transient_youtube_video_' . $parameters['id'];
	$items = get_transient($transient_name);
	if(!$items) {
		$items =array();
		$youtube_results = youtube_api($end_point, $parameters);
		$items = $youtube_results['items'];
		set_transient($transient_name, $items, 24*3600);
	}
	return $items;
}

// Add extra contenthub featured opotion page
if(function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title' => 'Site Settings',
		'menu_title' => 'Site Settings'
	));
}

function specific_before_init_insert_formats( $init_array ) {  
    // Define the style_formats array
    $style_formats = array(  
        // Each array child is a format with it's own settings
        array(  
            'title' => 'Button',  
            'selector' => 'a',  
            'classes' => 'btn btn-default'             
        )
    );  
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );  

    return $init_array;  

} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'specific_before_init_insert_formats' );

// For copy and paste youtube URL in content
add_filter('embed_oembed_html', 'tdd_oembed_filter', 10, 4) ;
function tdd_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="iframe-wrap">'.$html.'</div>';
    return $return;
}

// Function to cache results from instagram API
function get_instagram_items() {

	$transient_name = '_transient_instagram_items';

	$items = get_transient($transient_name);
	if(!$items) {
		$instagram_token = get_field('instagram_token', 'option');
		if(!$instagram_token) {
			return false;
		}
		$end_point = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $instagram_token;
		$instagram_results = wp_remote_get($end_point);
		$data = json_decode($instagram_results['body'], true);
		$data = isset($data['data']) ? $data['data'] : false;
		if($data) {
			$data = array_slice($data, 0, 12);
			set_transient($transient_name, $data, 3600);
			return $data;
		} else {
			return false;
		}
	} else {
		return $items;
	}

}

// Songkick pull function
function get_data_from_songkick() {


	$transient_name = '_transient_songkick_data';

	$data = get_transient($transient_name);

	if(!$data) {

		$songkick_artist_id = get_field('songkick_artist_id', 'option');
		$songkick_api_key = get_field('songkick_api_key', 'option');

		if($songkick_artist_id && $songkick_api_key) {
			$end_point = "http://api.songkick.com/api/3.0/artists/" . $songkick_artist_id . "/calendar.json?apikey=" . $songkick_api_key;
			$songkick_results = wp_remote_get($end_point);
			$data = json_decode($songkick_results['body'], true);
			$results = $data['resultsPage']['results'];

			if($results) {
				set_transient($transient_name, $results, 43200);
				return $results;
			}

		}

		return false;
	} else {
		return $data;
	}

}

function my_acf_init() {	
	acf_update_setting('google_api_key', get_field('google_api_key', 'option'));
}
add_action('acf/init', 'my_acf_init');
