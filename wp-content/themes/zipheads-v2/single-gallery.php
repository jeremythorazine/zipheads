<?php

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$templates = array( 'single-gallery.twig' );

Timber::render( $templates, $context );
