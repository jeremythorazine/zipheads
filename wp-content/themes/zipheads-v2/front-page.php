<?php
/**
 * The main front page template file
 *
 */



$context = Timber::get_context();
$context['post'] = Timber::get_post();

$gig = array(
    'post_type' => 'gig',
    'posts_per_page' => 1,
    'meta_key' => 'event_date', 
    'orderby' => 'meta_value_num',
    'order' => 'ASC',
    'meta_query' => array(array(
    	'key' => 'event_date',
    	'value' => date('Y-m-d'),
    	'compare' => '>=',
    	'type' => 'DATE'
    ))
);

$context['next_gig'] = get_data_from_songkick();
$context['next_gig'] = $context['next_gig']['event'][0];
$context['instagram_items'] = get_instagram_items();

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );
